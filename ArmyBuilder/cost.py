#!/usr/bin/env python3
from .html import dbg
from .data import rules_cost

"""
Copyright 2017 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


# Adjust defense and attack cost, to match onepagerules current prices
adjust_defense_cost = 1
adjust_attack_cost = 0.3


# Cost per defense point, +30% per defense point
def defense_factor(d):
    if d < 6:
        return (1.50 ** d) * (1.30 ** 6) / (1.50 ** 6)
    return 1.30 ** d


# Cost multiplier for a super defense
def super_defense_factor(defense, super_def):
    if not super_def:
        return 1
    # range of AP which will be affected by super defense
    ap_range = (8 - defense + super_def) / 7
    # efficiency of super defense
    super_proba = 5 / (7 - super_def) - 1
    if defense < 6:
        super_proba *= defense / 6
    return 1 + ap_range * super_proba


# probability to score higher or equal with a d6
# 5/6 for 2+, 1/6 for 6+
def dice6_stats(s):
    if s > 6:
        return 0.0
    elif s < 1:
        return 1.0
    return (7.0 - s) / 6.0


# AP cost multiplier.
# 1 for no AP, +20% per AP point
def ap_cost(ap):
    return 1.2 ** ap


# Damage cost multiplier
# high damage is less effective against small infantry units, so
# should be less expensive than just multiplying the price by the damage
# (1=>1, 2=> 1.74, 3=> 2.4, 4=> 3)
def damage_cost(d):
    return d ** 0.8


# Blast cost multiplier
# Blast weapons are inefficient if the units has less than X models
def blast_cost(b):
    return b ** 0.9


# Range cost multiplier.
# melee threaten range is charge distance (speed + 7")
# guns threaten range is advance distance (speed + weapon range)
# wrange > 48" doesn't add much threat in game.
def range_cost(wrange, speed):
    if wrange > 48:
        wrange = 48 + (wrange - 48.0) / 3.0
    if wrange == 0:
        trange = speed + 7
    else:
        trange = wrange + speed
    return trange ** 0.75


# Handle D3+1 or 2d6+2 values
# return the mean to calculate the cost
def dice_mean(value):
    if not "d" in str(value):
        return value

    n, rem = value.lower().split("d")
    if "+" in rem:
        sides, add = rem.split("+")
    else:
        sides, add = rem, 0

    mean = (int(sides) + 1) / 2.0
    if n:
        mean *= int(n)
    return mean + int(add)


def get_count(s):
    count, _, name = s.partition(" ")
    if count.endswith("x") and count[:-1].isdigit() and name:
        c = int(count[:-1])
        return c, name
    return 1, s


# Model special rules to weapon special rule translation table
model2weapon_sp = {
    "Accurate": "Accurate",
    "Agile": "Assault",
    "Precision": "Ignore Cover",
    "Initiative": "Initiative",
    "Interceptor": "Interceptor",
    "Multi-Tracker": "Multi-Tracker",
    "Rusher": "Rusher",
    "Gunship": "Gunship",
    "Weapon Beast": "Static Power",
}


def weapon_cost(model, weapon):
    sfactor = 1.0
    # Range > 84" is useless
    wrange = weapon.range if weapon.range < 84 else 84
    if wrange:
        tohit = model.bs
    else:
        tohit = model.cc
    ap = dice_mean(weapon.ap)
    attacks = dice_mean(weapon.attacks)
    damage = dice_mean(weapon.damage)
    speed = model.speed

    sp = []
    sp.extend(weapon.special)
    sp.extend(model.weapon_rules)
    sp = sorted(set(sp))

    for s in sp:
        if s == "Indirect":
            wrange *= 1.4
        elif s.startswith("Bomb("):
            tohit = int(s[5:-1])
            wrange = speed
            ap = 10
            attacks /= 2
        elif s.startswith("Blast("):
            blast = int(s[6:-1])
            sfactor *= blast_cost(blast)
        elif s == "Boom":
            attacks += 1
        elif s == "Assault":
            if weapon.range > 0:
                speed += 4
        elif s == "Static":
            # Can only fire if stationary
            speed = 0
            sfactor = 0.8
        elif s == "Turret":
            speed = 0
            sfactor = 0.4
        elif s == "Static Power":
            # Fire twice if stationary
            if wrange > 0:
                attacks *= 2
                speed = 0
        elif s == "Rusher":
            if wrange == 0:
                speed += 4
        elif s == "Anti-Air":
            # As most units are not flying, just count as no cost
            pass
        elif s == "Anti-Infantry":
            tohit -= 0.6
        elif s == "Multi-Tracker":
            tohit -= 0.5
        elif s == "Accurate":
            tohit -= 0.7
        elif s == "Interceptor":
            if weapon.range > 0:
                tohit -= 0.3
        elif s == "Gunship":
            if weapon.range > 0:
                tohit -= 0.7
        elif s == "Anti-Ground":
            tohit -= 0.3
        elif s == "Balanced":
            if tohit > 2:
                tohit -= 1
        elif s == "Flux":
            # Flux is statistically like having tohit -2
            tohit -= 2
        elif s == "Unbalanced":
            tohit += 1
        elif s == "Auto Hit":
            tohit = 1
        elif s == "Deff Rolla":
            tohit -= 3
        elif s == "Squig":
            tohit = 2
        elif s == "Ignore Cover":
            if wrange > 0:
                tohit -= 0.5
        elif s == "Sniper":
            # Sniper is ignore cover (so statistically half a tohit point)
            # and choosing enemy model, count as ap+1
            tohit -= 0.5
            ap += 1
        elif s == "Spotting":
            # Does no damage, but consider 1 attack at ap(3)
            ap = 3
        elif s == "Headshot":
            # Headshot is 1/6 of having AP + 3, and damage 1d3
            rending = (1 / 6) * (ap_cost(ap + 3) * damage_cost(2) - ap_cost(ap))
            sfactor *= 1 + rending / (ap_cost(ap) * dice6_stats(tohit))
        elif s == "Vehicle Destroyer":
            # like Headshot, on 5+ only on vehicle
            rending = (2 / 6) * (ap_cost(10) - ap_cost(ap))
            sfactor *= 1 + rending / (ap_cost(ap) * dice6_stats(tohit))
        elif s.startswith("Rending("):
            apboost = int(s[8:-1])
            # rending is 1/6 of having AP(+X)
            rending = (1 / 6) * (ap_cost(ap + apboost) - ap_cost(ap))
            sfactor *= 1 + rending / (ap_cost(ap) * dice6_stats(tohit))
        elif s.startswith("Impact("):
            # Impact add ap when charging
            simpact = int(s[7:-1])
            ap += simpact * 0.7
        elif s == "Overpower":
            ap += 3
        elif s == "Deadly":
            ap = 10
        elif s == "Ctan":
            # AP(8) against non-vehicle
            ap = (ap + 8) / 2
        elif s == "Psy Weapon":
            ap += 2
        elif s == "Urty":
            ap += 1.5
        elif s == "Chem":
            ap += 3
        elif s == "Deathstrike":
            ap = 10
            sfactor *= 0.8
        elif s == "Mandible":
            ap += 1
        elif s == "Smasha":
            ap *= 2
        elif s == "Kustom":
            # blow up on 1
            damage /= 2
        elif s == "Overcharge":
            # +1 damage with a risk to blow up
            damage += 0.5
        elif s == "Fusion":
            damage *= 1.15
        elif s == "Sublimation":
            damage *= 1.30
        elif s == "Graviton":
            damage = 1.5
        elif s == "Tellyportation":
            damage = 3
        elif s == "Traktor":
            damage += 1
        elif s == "Fire Charge":
            attacks += 0.7
        elif s == "Charge":
            sfactor *= 0.7
        elif s == "Rapid Fire":
            # double attacks at half range
            sfactor *= 1 + range_cost(wrange / 2, speed) / range_cost(wrange, speed)
        elif s == "Initiative":
            if wrange == 0:
                sfactor *= 1.2
        elif s == "Limited":
            sfactor /= 2
        elif s == "Target Spotted":
            # Limited + can fire only on marked target
            sfactor *= 0.3
        elif s == "Revenge":
            sfactor *= 1.2
        elif s == "Blind":
            sfactor *= 1.3
        else:
            raise Exception("Warning Weapon Special Rule {} Not handled".format(s))

    cost = sfactor * attacks * range_cost(wrange, speed)
    cost *= ap_cost(ap) * dice6_stats(tohit)
    cost *= damage_cost(damage) * adjust_attack_cost
    return cost


def mean_cost(costs):
    if len(costs) == 0:
        return 0
    if len(costs) == 1:
        return costs[0]
    return max(costs) + 0.25 * min(costs)


def combo_cost(model, combo):
    ranged_costs = [
        weapon_cost(model, weapon)
        for weapon in combo.profiles.values()
        if weapon.range > 0
    ]
    close_costs = [
        weapon_cost(model, weapon)
        for weapon in combo.profiles.values()
        if weapon.range == 0
    ]
    return mean_cost(ranged_costs) + mean_cost(close_costs)


def wargear_cost(model, wargear):
    return wargear.add_cost


class ModelCost:
    def __init__(self, model):
        self.speed = model["speed"]
        self.cc = model["cc"]
        self.bs = model["bs"]
        self.defense = model["defense"]
        self.hitpoints = model["hitpoints"]
        self.special = model["special"]
        self.super_defense = 0
        self.transport = 0
        self.charge_speed = 7
        self.global_add = 0
        self.global_mul = 0
        self.psychic = 0
        self.regeneration = 0
        self.firing_deck = 0
        self.weapon_rules = []
        self.wargear_rules = []
        self.rules = []

    # return additionnal weapons rules given by models rules
    # (Like Agile => Assault for all weapons)
    def apply_weapon_rules(self, equipments):
        rules = []
        for r in self.special:
            if not r in model2weapon_sp:
                continue
            rules.append(model2weapon_sp[r])
        for r in self.wargear_rules:
            if not r in model2weapon_sp:
                continue
            rules.append(model2weapon_sp[r])
        self.weapon_rules = rules

    # Return additionnal models rules given by wargears
    def apply_wargear_rules(self, equipments):
        rules = []
        for wargear in equipments:
            if not hasattr(wargear, "rules"):
                continue
            for r in wargear.rules:
                if not r in rules:
                    rules.append(r)
        self.wargear_rules = rules

    def parse_characteristic(self, rule):
        char = [
            "Defense",
            "CC",
            "BS",
            "Speed",
            "HP",
            "Transport",
            "Psychic",
            "Super Defense",
            "Regeneration",
            "Firing Deck",
        ]
        for c in char:
            if rule.startswith(c):
                option = rule[len(c) :]
                if not option:
                    raise ValueError(
                        f"Syntax error in special Rule {rule} for unit {self.name}"
                    )
                attr = c.lower().replace(" ", "_")
                if attr == "hp":
                    attr = "hitpoints"
                if option.startswith("("):
                    setattr(self, attr, int(option[1:-1]))
                elif option.startswith("+"):
                    value = getattr(self, attr) + int(option[1:])
                    setattr(self, attr, value)
                elif option.startswith("-"):
                    value = getattr(self, attr) - int(option[1:])
                    setattr(self, attr, value)
                else:
                    return False
                return True
        return False

    def apply_rules(self, equipments):
        self.apply_wargear_rules(equipments)
        self.apply_weapon_rules(equipments)
        self.rules = sorted(set(self.special + self.wargear_rules))  # __:opov

        hp_mul = 1
        speed_mul = 1
        transport_mul = 1

        for s in self.rules:
            if self.parse_characteristic(s):
                continue
            elif s == "Stealth":
                # Stealth is like +0.5 def, because it works only against ranged attack
                self.defense += 0.5
            elif s == "Smoke Launchers":
                self.defense += 0.3
            elif s == "War Shout":
                self.defense += 0.5
            elif s == "Tesla Shield":
                self.defense += 2
            elif s == "Terror":
                # Terror is like +0.5 def, because it works only against Close Combat attack
                self.defense += 0.5
            elif s == "Living Metal":
                self.hitpoints += 2
            elif s == "Ambush":
                self.global_mul += 0.20
            elif s == "Scout":
                self.global_mul += 0.15
            elif s == "Interception":
                self.global_mul += 0.5
            elif s == "Monster Hunter":
                self.cc -= 0.7
                self.bs -= 0.7
            elif s == "Tank Hunter":
                self.bs -= 0.5
            elif s == "Ammo Runt":
                self.bs -= 0.3
            elif s == "Dakka":
                self.bs -= 0.5
            elif s == "Dodge":
                self.super_defense = 3
            elif s == "Energy Shield":
                hp_mul *= 1.3
            elif s == "Flying":
                speed_mul *= 1.1
            elif s == "Super Fast":
                self.speed += 2
            elif s == "Waaagh!" or s == "Swift":
                self.speed += 2
            elif s == "Turbo":
                self.speed += 2
            elif s == "Reanimation":
                self.hitpoints += 0.5
            elif s == "Aircraft":
                # Aircrafts have severe movement restriction
                speed_mul /= 2.5
            elif s == "Hybrid":
                speed_mul *= 1.7
            elif s == "Hit and Run":
                speed_mul *= 2
            elif s == "Track Guards":
                speed_mul *= 1.2
            elif s == "Repair":
                self.global_add += 4 * defense_factor(8)
            elif s == "Open-Topped":
                transport_mul *= 1.5
            elif s in ["Hero", "Vehicle", "Monster"]:
                continue
            elif s in model2weapon_sp:
                continue
            elif s in rules_cost:
                continue
            else:
                raise Exception(f"rules not handled {s}")

        self.global_add += 25 * self.psychic
        self.global_add += 3 * self.firing_deck
        if self.super_defense:
            if self.super_defense not in [3, 4, 5]:
                raise ValueError(" Super Defense should be either 3+, 4+ or 5+")
            if self.defense < self.super_defense:
                self.defense = self.super_defense
        if self.regeneration:
            hp_mul *= 1 / (1 - (dice6_stats(self.regeneration)))
        self.hitpoints *= hp_mul
        self.speed *= speed_mul
        self.transport *= transport_mul

    def attack_cost(self, equipments):
        return sum([w.cost(self) for w in equipments])

    @property
    def defense_cost(self):
        cost = defense_factor(self.defense) * self.hitpoints

        if self.super_defense:
            cost *= super_defense_factor(self.defense, self.super_defense)
        # include speed to defense cost. hardened target which move fast are critical to control objectives.
        cost *= (self.speed + 30) / 36
        return cost * adjust_defense_cost

    def total_cost(self, equipments):
        attack_cost = self.attack_cost(equipments)
        defense_cost = self.defense_cost

        other_cost = self.global_add
        # For transporter, the cost depends on defense, and speed
        other_cost += self.transport * (defense_cost / 150) * (self.speed / 6)
        other_cost += (attack_cost + defense_cost) * self.global_mul

        faction_cost = sum([rules_cost[r] for r in self.rules if r in rules_cost])
        # dbg(
        #   f"Cost\tA:{attack_cost:.2f}\tD:{defense_cost:.2f}\tO:{other_cost:.2f}\tF:{faction_cost:.2f}"
        # )
        return attack_cost, defense_cost, other_cost, faction_cost

    def cost(self, equipments):
        self.apply_rules(equipments)
        return sum(self.total_cost(equipments))


# To calculate cost with multiple models with different
# equipment, dispatch stuff between them.
def dispatch_equ(count, equipments):
    equlists = [[] for _ in range(count)]
    i = 0
    for e, c in equipments:
        for _ in range(c):
            equlists[i % count].append(e)
            i += 1

    # for equ in equlists:
    #    dbg(f"Model:")
    #    for e in equ:
    #        dbg(f"E {e.name}")
    return equlists


def model_cost(count, model, equipments):
    equlists = dispatch_equ(count, equipments)
    cost = sum([ModelCost(model).cost(e) for e in equlists])
    return round(cost)

#!/usr/bin/env python3

"""
Copyright 2017 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import argparse
import os
from pathlib import Path

import yaml

# from ArmyBuilder.cost import ModelCost, combo_cost, wargear_cost, weapon_cost

numbers = [
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
    "ten",
]


class Model:
    def __init__(
        self, name, speed, cc, bs, defense, hitpoints=1, equipment=None, special=None
    ):
        self.name = name
        self.speed = speed
        self.cc = cc
        self.bs = bs
        self.defense = defense
        self.hitpoints = hitpoints
        self.equipment = equipment or []
        self.special = special or []
        self.upgrades = []

    def to_list(self):
        return [
            self.speed,
            self.cc,
            self.bs,
            self.defense,
            self.hitpoints,
            self.equipment,
            self.special,
            [u.to_dict() for u in self.upgrades],
        ]


class Upgrade:
    def __init__(
        self,
        add,
        models,
        text="",
        remove=None,
        all=False,
        max=0,
        max_model=0,
        need=None,
    ):
        self.add = add
        self.remove = remove or []
        self.all = all
        self.max = max
        self.max_model = max_model
        self.need = need or []
        self.text = text if text else self._get_text()

    @property
    def _get_count(self):
        if self.max_model:
            if isinstance(self.max_model, str):
                n, _, d = self.max_model.partition("/")
                n = int(n)
                d = int(d)
                return f"up to {n} for every {d} models"
            return f"up to {numbers[self.max_model]} per model"
        if not self.max:
            return ""
        if self.max == 1:
            return "one"
        if self.max == -1:
            return "any"
        return "up to " + numbers[self.max]

    # Generate upgrade text
    def _get_text(self):
        if self.remove:
            sremove = " and ".join(self.remove)
            if self.all:
                return f"Replace all {sremove}"
            return f"Replace {self._get_count} {sremove}"
        if self.all:
            return f"Upgrade all models with {self._get_count}"
        return f"Upgrade with {self._get_count}"

    def to_dict(self):
        default = {"remove": [], "all": False, "max": 0, "max_model": 0, "need": []}
        ret = {}
        for k, v in self.__dict__.items():
            if k in default and default[k] == v:
                continue
            ret[k] = v
        return ret


def write_file(fname, data):
    with open(fname, "w") as f:
        print("  Writing {}".format(fname))
        f.write(data)


def read_yaml(faction, filename):
    fname = Path("Factions")
    if faction:
        fname = fname / faction
    fname = fname / (filename + ".yml")
    if not fname.exists():
        return None
    with fname.open("r") as f:
        print("  Processing {}".format(fname))
        return yaml.safe_load(f.read())


def convert_weapon(data):
    range = data.get("range", 0)
    attacks = data.get("attacks", 1)
    ap = data.get("ap", 0)
    damage = data.get("damage", 1)
    special = data.get("special", [])
    return [range, attacks, ap, damage, special]


class Armory(dict):
    def __init__(self):
        self["weapons"] = {}
        self["combos"] = {}
        self["wargears"] = {}

    def add_weapons(self, weapons):
        for w, data in weapons.items():
            if "@" in w:
                name, _, profile = w.partition("@")
                if name not in self["combos"]:
                    self["combos"][name] = {profile: convert_weapon(data)}
                else:
                    self["combos"][name][profile] = convert_weapon(data)
            else:
                self["weapons"][w] = convert_weapon(data)

    def add_wargears(self, wargears):
        for w, data in wargears.items():
            self["wargears"][w] = data


def gen_armory(factions):
    armory = Armory()
    for d in [""] + factions:
        yml = read_yaml(d, "equipments")
        armory.add_weapons(yml.get("weapons"))
        armory.add_wargears(yml.get("wargear"))
    return armory


def gen_faction_barracks(faction):
    yml_model = read_yaml(faction, "models")
    models = {name: Model(name, **data) for name, data in yml_model.items()}
    yml_upgrades = read_yaml(faction, "upgrades")
    for up in yml_upgrades:
        for model in up["models"]:
            models[model].upgrades.append(Upgrade(**up))
    return {model: data.to_list() for model, data in models.items()}


def gen_barracks(factions):
    return {faction: gen_faction_barracks(faction) for faction in factions}


def gen_units(factions):
    return {faction: read_yaml(faction, "units") for faction in factions}


def gen_rules(factions):
    rules = {}
    for faction in [""] + factions:
        rules.update(read_yaml(faction, "rules")["specialRules"])
    return rules


def gen_rules_cost(factions):
    rules = {}
    for faction in factions:
        rules.update(read_yaml(faction, "rules")["rulesCost"])
    return rules


def gen_psychics(factions):
    return {faction: read_yaml(faction, "psychics") for faction in factions}


def gen_army_builder(factions):
    data = {
        "factions": factions,
        "barracks": gen_barracks(factions),
        "armory": gen_armory(factions),
        "units": gen_units(factions),
        "rules": gen_rules(factions),
        "rules_cost": gen_rules_cost(factions),
        "psychics": gen_psychics(factions),
    }
    out = "\n".join([k + " = " + str(v) for k, v in data.items()])
    write_file("ArmyBuilder/data.py", out)


def get_factions():
    # Faction order required to keep old army list with ArmyBuilder
    factions = [
        "Astra Militarum",
        "Craftworlds",
        "Necrons",
        "Orks",
        "Space Marines",
        "Tau",
        "Tyranids",
    ]

    for dir in os.scandir("Factions"):
        if not dir.is_dir():
            continue
        files = os.scandir(dir)
        if "models.yml" in [f.name for f in files] and dir.name not in factions:
            factions.append(dir.name)

    print("all factions:", ", ".join(factions))
    return factions


def main():
    parser = argparse.ArgumentParser(
        description="This script will compute the Unit costs and generate a data file for ArmyBuilder"
    )
    parser.add_argument(
        "path",
        type=str,
        nargs="*",
        help="path to the faction (should contain at least equipments.yml, units.yml, upgrades.yml, faction.yml)",
    )
    parser.add_argument("-a", "--army-builder", type=str, default="ArmyBuilder")

    args = parser.parse_args()

    if args.path:
        factions = args.path
    else:
        factions = get_factions()

    gen_army_builder(factions)


if __name__ == "__main__":
    # execute only if run as a script
    main()

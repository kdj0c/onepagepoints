#!/usr/bin/env python3

"""
Copyright 2019 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

from sys import exit
import yaml

# global data dict: factions, barracks, armory, units, rules, rules_cost, psychics
from ArmyBuilder.data import factions, barracks, units
from ArmyBuilder.unit import Models

errors = []


def check_models_units(f):
    models = barracks[f]
    all_models = models.keys()
    all_miu = []
    for funits in units[f].values():
        for name, composition in funits.items():
            if composition == "self":
                all_miu.append(name)
                continue
            for c in composition:
                for m in c["models"]:
                    all_miu.append(m)
    m1 = set(all_models)
    m2 = set(all_miu)
    missing = m2 - m1
    if missing:
        errors.append(
            f"{f} : models {missing} are used in units.yml but not defined in models.yml"
        )
    unused = m1 - m2
    if unused:
        errors.append(
            f"{f} : models {unused} are defined in models.yml, but not referenced in any unit"
        )


def write_model_cost(fd, m):
    m.currentcost = m.cost
    columns = [
        ["speed", '"'],
        ["cc", ""],
        ["bs", ""],
        ["defense", "+"],
        ["hitpoints", ""],
    ]
    char = " ".join([str(m.template[k]) + s for k, s in columns])
    fd.write(f"{m.name}: {char} {m.currentcost} pts\n")
    for up in m.up_template:
        for equ in up.add:
            cost = m.upgrade_cost(up, equ)
            fd.write(f'\t-> {", ".join(equ)} {cost} pts\n')


def check_cost():
    with open("cost.txt", "w") as fd:
        for f in factions:
            for name, model in barracks[f].items():
                m = Models(1, name, model)
                write_model_cost(fd, m)


def main():
    for f in factions:
        check_models_units(f)
    for e in errors:
        print(e)
    check_cost()
    if errors:
        exit(1)


if __name__ == "__main__":
    # execute only if run as a script
    main()

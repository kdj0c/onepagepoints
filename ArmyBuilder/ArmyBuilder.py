from base64 import Symbol, char2symbol, symbol2char, to_symbol
from html import dbg, htmlAttrib, htmlClass, htmlTag
from data import barracks, factions, psychics, rules, units
from unit import Unit


def option(name):
    return htmlAttrib("option", f'value="{name}"', name.replace("_", " "))


def drag(event, id):
    event.dataTransfer.setData("swap", id)


def allowdrop(event):
    event.preventDefault()


def drop(event, id):
    event.preventDefault()
    data = event.dataTransfer.getData("swap")
    dbg(f"drag from {data} to {id}")
    army_builder.armyBuilder.Swap(data, id)


def parse_url(search):
    search = decodeURIComponent(search)
    if search.startswith("?"):
        search = search[1:]
    search = search.split("#")[0]
    params = {}
    for p in search.split("&"):
        k, v = p.split("=")
        params[k] = v
    return params


def clean_rule(rule):
    if rule.endswith(")"):
        rule = rule[:-3]
        if rule.endswith("("):
            rule = rule[:-1]
        rule += "(X)"
    return rule


class ArmyBuilder:
    def __init__(self, params):
        self.sections = {}
        self.units = []
        self.models = []
        self.army = []
        self.name = ""
        self.faction_list()
        self.section_list()
        self.unit_list()
        self.u2id = self.unit_inventory
        self.faction = ""
        self.fid = -1
        self.current_unit = None
        self.current_model = None
        self.parse_param(params)

    def parse_param(self, params):
        params = parse_url(params)
        if "name" in params:
            document.getElementById("army_name").value = params["name"]
            self.name = params["name"]
        if "army" in params:
            self.load_from_b64(params["army"])

    def name_changed(self):
        self.name = document.getElementById("army_name").value
        self.update_url()

    def update_url(self):
        p = []
        if self.name != "":
            p.append("name={}".format(encodeURIComponent(self.name)))
        if len(self.army) > 0:
            p.append("army=" + self.to_base64())
        window.history.pushState("unused", "Army name update", "?" + "&".join(p))

    def load_unit(self, uid):
        name = self.u2id[self.faction][uid]
        template = [
            template
            for section in self.sections.values()
            for n, template in section.items()
            if n == name
        ][0]
        new_unit = Unit(name, template, self.models, self.fid, uid)
        self.army.append(new_unit)
        self.current_unit = new_unit

    def load_faction(self, fid):
        self.fid = fid
        self.faction = factions[fid]
        self.sections = units[self.faction]
        self.models = barracks[self.faction]

    def load_model(self, count, mid):
        self.current_model = self.current_unit.load_model(count, mid)

    def set_selections(self):
        if not self.faction:
            return
        document.getElementById("faction_list").value = self.faction
        self.faction_selected()
        for section, templates in self.sections.items():
            if self.current_unit.name in templates.keys():
                document.getElementById("sections_list").value = section
                self.section_selected()
                document.getElementById("units_list").value = self.current_unit.name
                return

    def load_from_b64(self, army_b64):
        self.army = []
        symbols = char2symbol(army_b64)
        if not symbols:
            return
        count = 1
        for s in symbols:
            symb = Symbol(s)
            if symb.type == "faction":
                self.load_faction(symb.value)
            elif symb.type == "unit":
                self.load_unit(symb.value)
            elif symb.type == "count":
                count = symb.value
            elif symb.type == "model":
                self.load_model(count, symb.value)
                count = 1
            elif symb.type == "upgrade":
                self.current_model.load_upgrade(count, symb.value)
                count = 1

        self.set_selections()
        self.display_army_list()

    def faction_list(self):
        # Copy the factions_name before sorting it in place
        flist = [f for f in factions]
        flist.sort()
        html_list = htmlAttrib(
            "option", 'value="disabled" disabled selected', "Select Faction"
        )
        html_list += " ".join([option(faction) for faction in flist])
        document.getElementById("faction_list").innerHTML = html_list

    def section_list(self):
        html_list = htmlAttrib(
            "option", 'value="disabled" disabled selected', "Select Section"
        )
        html_list += " ".join([option(section) for section in self.sections])
        document.getElementById("sections_list").innerHTML = html_list

    def unit_list(self):
        section = document.getElementById("sections_list").value
        html_list = htmlAttrib(
            "option", 'value="disabled" disabled selected', "Select Unit"
        )
        if section != "disabled":
            self.units = self.sections[section]
            html_list += " ".join([option(unit) for unit in self.units])
        document.getElementById("units_list").innerHTML = html_list

    def faction_selected(self):
        self.faction = document.getElementById("faction_list").value
        self.fid = factions.index(self.faction)
        self.sections = units[self.faction]
        self.models = barracks[self.faction]
        self.section_list()
        self.unit_list()

    def section_selected(self):
        self.unit_list()

    def Add(self):
        new_name = document.getElementById("units_list").value
        if new_name == "disabled":
            return
        unit_template = self.units[new_name]
        uid = self.u2id[self.faction].index(new_name)
        new_unit = Unit(new_name, unit_template, self.models, self.fid, uid)
        new_unit.base_models()
        self.army.append(new_unit)
        self.display_army_list()

    def Delete(self, id_str):
        index = id_str[7:]
        self.army.pop(int(index))
        self.display_army_list()

    def Swap(self, pos1, pos2):
        unit = self.army.pop(int(pos1))
        self.army.insert(int(pos2), unit)
        self.display_army_list()

    def Composition(self, id_str):
        _, unit, _ = id_str.split("_")
        self.current_unit = self.army[unit]
        self.display_composition_list()

    def Edit(self, id_str):
        _, unit, model = id_str.split("_")
        self.current_unit = self.army[unit]
        self.current_model = self.current_unit.models[model]
        self.display_upgrade_list()

    def UpEdit(self, id_str):
        _, ml, m = id_str.split("_")
        self.current_model = self.current_unit.model(ml, m)
        self.display_upgrade_list()

    def display_army_list(self):
        document.getElementById("army_list").innerHTML = self.get_army_table()
        document.getElementById("total").innerHTML = self.get_total()
        document.getElementById("rules").innerHTML = self.get_rules()
        document.getElementById("psychics").innerHTML = self.get_psychics()
        self.update_url()

    def display_composition_list(self):
        document.getElementById("army_list").innerHTML = self.get_composition()
        document.getElementById("total").innerHTML = self.get_total()
        document.getElementById("rules").innerHTML = self.get_rules()
        document.getElementById("psychics").innerHTML = self.get_psychics()

    def display_upgrade_list(self):
        document.getElementById("army_list").innerHTML = self.get_upgrade()
        document.getElementById("total").innerHTML = self.get_total()
        document.getElementById("rules").innerHTML = self.get_rules()
        document.getElementById("psychics").innerHTML = self.get_psychics()

    def CompAdd(self, key):
        dbg(f"add {key}")
        _, ml, m = key.split("_")
        self.current_unit.model_list[ml].increase(m)
        self.display_composition_list()

    def CompDel(self, key):
        dbg(f"remove {key}")
        _, ml, m = key.split("_")
        self.current_unit.model_list[ml].decrease(m)
        self.display_composition_list()

    def get_total(self):
        total = sum([unit.cost for unit in self.army])
        return str(total) + " pts"

    def get_army_table(self):
        mini_header = ["Sp", "CC", "BS", "Df", "HP"]
        mini_header = [htmlClass("div", "mcell", mt) for mt in mini_header]
        mrow = htmlClass("div", "mrow", "".join(mini_header))
        mtable = htmlClass("div", "mtable", mrow)
        header = [mtable, "Equipments", "Special Rules", "Cost", "edit", "delete"]
        header = [htmlClass("div", "dcell", t) for t in header]
        rows = [htmlClass("div", "drow", "".join(header))]
        rows.extend(
            [row for n, unit in enumerate(self.army) for row in unit.al_rows(n)]
        )
        return htmlClass("div", "dtable", "".join(rows))

    def UpAdd(self, id_str):
        _, gid, n = id_str.split("_")
        self.current_model.add_upgrade(gid, n, 1)
        self.display_upgrade_list()

    def UpDel(self, id_str):
        _, gid, n = id_str.split("_")
        self.current_model.del_upgrade(gid, n)
        self.display_upgrade_list()

    def Up_Done(self, id_str):
        document.getElementById("add_unit").style.display = "flex"
        self.display_army_list()

    def get_composition(self):
        document.getElementById("add_unit").style.display = "none"
        return self.current_unit.composition_menu

    def get_upgrade(self):
        document.getElementById("add_unit").style.display = "none"
        return self.current_model.upgrade_menu

    def get_rules(self):
        def fmt_rule(r):
            return htmlTag("li", htmlTag("strong", r + ":") + " " + rules[r])

        all_rules = set([clean_rule(r) for unit in self.army for r in unit.rules])
        html_data = "".join([fmt_rule(r) for r in sorted(all_rules) if r in rules])
        return htmlTag("h3", "Special Rules") + htmlTag("ul", html_data)

    def _get_psy_power(self, name, power, desc):
        return htmlTag(
            "li", htmlTag("strong", name + " (" + str(power) + "+): ") + desc
        )

    def get_psychics(self):
        psy = {}
        for unit in self.army:
            for rule in unit.rules:
                if (
                    rule.startswith("Psychic(")
                    or rule == "Council"
                    or rule == "Group Psychic"
                ):
                    faction = factions[unit.fid]
                    if faction not in psy:
                        psy[faction] = psychics[faction]

        data = ""
        for faction, powers in psy.items():
            title = htmlTag("h3", "Psychic Spells for " + htmlTag("i", faction))
            rows = [
                self._get_psy_power(name, power, desc)
                for power, spell in powers.items()
                for name, desc in spell.items()
            ]
            data += title + htmlTag("ul", "".join(rows))
        return data

    @property
    def unit_inventory(self):
        ret = {}
        for faction, funits in units.items():
            ret[faction] = [
                name for section in funits.values() for name in section.keys()
            ]
        return ret

    def to_base64(self):
        c_fid = -1
        symbols = []
        for unit in self.army:
            if unit.fid != c_fid:
                symbols.append(to_symbol(unit.fid, "faction"))
                c_fid = unit.fid
            symbols.extend(unit.symbols)
        b64 = symbol2char(symbols)
        return b64


armyBuilder = ArmyBuilder(window.location.search)

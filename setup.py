from cx_Freeze import Executable, setup

setup(
    name="faction",
    version="0.1",
    description="Generate faction html for fasthammer",
    executables=[Executable("faction.py")],
)

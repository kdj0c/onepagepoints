#!/usr/bin/env python3

"""
Copyright 2019 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""

import argparse

import markdown
import yaml
from markdown.extensions.toc import TocExtension
from ArmyBuilder.armory import Weapon, Combo


def get_sp_rules():
    with open("Factions/rules.yml") as f:
        data = yaml.safe_load(f.read())
    sp_rules = data["specialRules"]

    return "\n".join([f"* **{k}:** {v}" for k, v in sp_rules.items()])


def get_weapons():
    weapons = [
        Weapon("knife", [0, 2, 0, 1, []]),
        Weapon("Rifle", [24, 1, 0, 1, []]),
        Weapon("MegaAxe", [0, 8, 5, 3, ["Balanced"]]),
        Weapon("MegaGun", [48, "2d3", 3, "1d3", ["Assault"]]),
        Combo(
            "Combo", {"Focused": [48, 1, 6, "1d6", []], "Frag": [36, "2d6", 1, 1, []]}
        ),
    ]

    whtml = [f"<li><b>{w.name}</b> {w.html()}</li>" for w in weapons]
    return "<ul>" + "".join(whtml) + "</ul>"


def get_html_from_markdown():
    with open("Common/rules.md") as f:
        data = f.read()
    data = data.replace("### SPECIAL RULES GENERATED LATER ###", get_sp_rules())
    data = data.replace("### WEAPONS GENERATED LATER ###", get_weapons())
    return markdown.markdown(
        data, extensions=["markdown.extensions.tables", TocExtension(marker="")]
    )


def main():
    parser = argparse.ArgumentParser(
        description="generate special rules in markdown, and convert it to html"
    )
    parser.add_argument(
        "outfile", metavar="outfile", type=str, help="path the generated html file"
    )
    args = parser.parse_args()

    with open("Template/header.html") as f:
        out = f.read()
    out += get_html_from_markdown()
    with open("Template/footer.html") as f:
        out += f.read()
    with open(args.outfile, "w") as f:
        f.write(out)


if __name__ == "__main__":
    # execute only if run as a script
    main()

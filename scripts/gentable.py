#!/usr/bin/env python3

"""
Copyright 2017 Jocelyn Falempe kdj0c@djinvi.net

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software is furnished to do so,
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""


def def_formula(t, svg):
    r = round(0.9 * t ** 0.78 + (8 - svg) * 0.7 + 0.2)
    return str(r) + "+"


def ap_formula(f, ap):
    r = round(1.25 * f ** 0.7 - 0.7 * ap - 3.3)
    return min(7, max(0, r))


class ConvTable:
    def __init__(self, fml, rx, ry, title, col_width=2, quirck=""):
        self.fml = fml
        self.rx = rx
        self.ry = ry
        self.cw = col_width
        self.quirck = quirck
        self.table = [[title] + [self.cell(x) for x in rx]]
        self.table += [self.get_row(y) for y in ry]

    def cell(self, n, cw=None):
        if not cw:
            cw = self.cw
        fmt = "{:^" + str(cw) + "}"
        return fmt.format(str(n))

    def get_row(self, y):
        return [str(y) + self.quirck] + [self.cell(self.fml(x, y)) for x in self.rx]

    def prt(self):
        for r in self.table:
            row = self.cell(r[0], 6) + " || " + " | ".join(r[1:]) + " |"
            print(row)
            print("_" * len(row))


def main():
    def_table = ConvTable(def_formula, range(1, 11), range(6, 1, -1), "Svg\\T", 3, "+")
    ap_table = ConvTable(ap_formula, range(1, 15), range(0, -6, -1), "AP\\F")

    print("Conversion Armor Save + Toughness -> Defense")
    print()
    def_table.prt()
    print("\n\n")
    print("Conversion AP + Stength -> AP")
    print()
    ap_table.prt()
    print()


if __name__ == "__main__":
    # execute only if run as a script
    main()
